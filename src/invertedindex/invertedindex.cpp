#include <iostream>
#include <filesystem>
#include <fstream>
#include <vector>
#include <thread>
#include <regex>
#include <map>
#include <mutex>
#include <chrono>

using namespace std;
namespace fs = std::filesystem;

class invertedIndex
{
public:
    void addTable(multimap <string, fs::path>* localIndex) {
        mtx.lock();
        this->index.insert(localIndex->begin(), localIndex->end());
        cout << "inserting!" << endl;
        mtx.unlock();
    }
    void search(string key) {
        cout << "Searching for " << key << endl;
        auto range = this->index.equal_range(key);
        if (range.first == range.second) {
            cout << "Nothing was found!" << endl;
        }
        for (auto it = range.first; it != range.second; it++) {
            cout << it->first << " " << it->second.string() << endl;
        }
    }
private:
    multimap<string, fs::path> index;
    mutex mtx;
};

class File
{
public:
    fs::path path;
    std::string data;
};

void threadfunc(unsigned startPos, unsigned endPos, vector<File>* files, invertedIndex* index) {
    multimap <string, fs::path> localIndex;
    cout << "Thread" << " : " << startPos
        << " - " << endPos << endl;
    auto words_end = std::sregex_iterator();
    std::regex words_regex("[^\\s.,/:\"';( -)<>!?]+");
    for (unsigned file = startPos; file != endPos; file++) {
        auto words_begin = std::sregex_iterator(files->at(file).data.begin(), files->at(file).data.end(), words_regex);
        for (std::sregex_iterator wordIt = words_begin; wordIt != words_end; ++wordIt) {
            localIndex.insert(pair<string, fs::path>(wordIt->str(), files->at(file).path));
        }
    }
    index->addTable(&localIndex);
}

int main() {
    fs::path path("C:\\po_coursach\\data");
    vector<File>* files = new vector<File>;
    File tmpFile;
    ifstream infile;
    for (auto& p : fs::directory_iterator(path)) {
        std::cout << p.path() << std::endl;
        infile.open(p.path());
        tmpFile.path = p.path();
        tmpFile.data = { istreambuf_iterator<char>(infile), istreambuf_iterator<char>() };
        infile.close();
        files->insert(files->begin(), tmpFile);
    }
    int NumThreads;
    cout << "Enter N thread" << endl;
    cin >> NumThreads;
    cout << NumThreads << endl;
    invertedIndex* index = new invertedIndex;
    std::thread* threads = new std::thread[NumThreads];
    auto start = chrono::steady_clock::now();
    for (unsigned i = 0; i < NumThreads; i++) {
        unsigned startpos = float(files->size()) / NumThreads * i;
        unsigned endpos = float(files->size()) / (NumThreads) * (i + 1);
        threads[i] = std::thread(threadfunc, startpos, endpos, files, index);
    }
    for (unsigned i = 0; i < NumThreads; i++) {
        threads[i].join();
    }
    auto end = chrono::steady_clock::now();
    cout << std::chrono::duration_cast<std::chrono::milliseconds>(end -
        start).count() << "ms" << endl;
    string key;
    while (true) {
        cout << "Enter keyword" << endl;
        cin >> key;
        index->search(key);
    }
    return 0;
}
